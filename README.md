Ler até ao fim antes de tentar executar. 

## RestSSL 
A classe RestSSL inclui um exemplo de como fazer os request em SSL, adicionando o contexto ao Client (resteasy). Nesta situação terão de verificar o certificado programáticamente. Devem passar pelo setup do ssl e gerar um certificado para posterior verificação. Docs:

https://docs.oracle.com/cd/E19900-01/819-4733/6n6s6u1gl/index.html

http://www.eclipse.org/jetty/documentation/current/configuring-ssl.html

## Form login com java realm security 
Exemplo de form login e autorization para realm no wildfly. A intercepção terá de ser feita ao nível da servlet. Combinando a servlet com um bean, temos mais liberdade para efectuar "lógica" antes ou depois de efectuar login e autorização.

Baseado nesta solução (um bocado diferente):

https://github.com/wildfly/quickstart/tree/11.x/servlet-security


Configurações do standalone:

Para debug na consola de todo o processo de segurança
```xml
<subsystem xmlns="urn:jboss:domain:logging:3.0">
	...  
<logger category="org.jboss.security">
                <level name="TRACE"/>
                <handlers>
                    <handler name="CONSOLE_TRC"/>
                </handlers>
            </logger>
    ...
```
Parte de segurança
```xml
<subsystem xmlns="urn:jboss:domain:security:2.0">
	...
            <security-domains>
                <security-domain name="secureDomain" cache-type="default">
                    <authentication>
                        <login-module code="Database" flag="required">
                            <module-option name="dsJndiName" value="java:/UserDS"/>
                            <module-option name="principalsQuery" value="select passwd from Users where username=?"/>
                            <module-option name="rolesQuery" value="select role, 'Roles' from UserRoles where username=?"/>
                            <module-option name="hashAlgorithm" value="SHA-256"/>
                            <module-option name="hashEncoding" value="base64"/>
                        </login-module>
                    </authentication>
                </security-domain>
    ...
```

Assumindo o seguinte datasource
```xml
<datasource jta="true" jndi-name="java:/UserDS" pool-name="UserDS" enabled="true" use-ccm="false">
                    <connection-url>jdbc:mysql://localhost:3306/realmtest</connection-url>
                    <driver-class>com.mysql.jdbc.Driver</driver-class>
                    <driver>mysql-connector-java-5.1.46.jar_com.mysql.jdbc.Driver_5_1</driver>
                    <security>
                        <user-name>root</user-name>
                        <password>12345678</password>
                    </security>
</datasource>
```

E a seguinte base de dados:
```sql
CREATE TABLE Users(username VARCHAR(255) PRIMARY KEY, passwd VARCHAR(255))
CREATE TABLE UserRoles(username VARCHAR(255), role VARCHAR(32)) 
-- username: admin  pw: admin
INSERT into USERS values('admin', 'jGl25bVBBBW96Qi9Te4V37Fnqchz/Eu4qB9vKrRIqRg=');
-- username: user   pw: user
INSERT into USERS values('user', 'BPiZbadjt6lpsQKO4wB1aerzpjVIbdqyEdUSyFud+Ps=');

INSERT into UserRoles values('admin', 'Manager');
INSERT into UserRoles values('user', 'User');
```

Precisamos da servelet API no pom.xml:
```xml
	<dependencies>
		...
			<dependency>
	            <groupId>org.jboss.spec.javax.servlet</groupId>
	            <artifactId>jboss-servlet-api_3.1_spec</artifactId>
	            <scope>provided</scope>
	        </dependency>
	    ...
	</dependencies>
```

Nas propriedades do projeto (alt+enter ou click do lado direito no projeto) precisamos de colocar o "Targeted Runtimes" para o servidor wildfly. Desta forma o eclipse deve conseguir encontrar as classes do javax.servlet.*  .


