package realmtester;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

public class RestSSL {

	private static final String TRUSTSTORE_FILE = "client-truststore.jks";
    private static final String TRUSTSTORE_PASSWORD = "password";
    // exemplo..
    private static final String APP_URL = "https://localhost:8443/secured-rest-app/webapi/myresource";
	
	public static void main(String args[]) throws Exception {
///////// Como aceder ao keystore programaticamente ///////
//		//SSLContext sslContext = SSLContext.getInstance("TLS"); 
//		//sslContext.init(null, new TrustManager()[], null);
//		//ClientBuilder.newBuilder().sslContext(sslContext).hostNameVerifier(hostNameVierifer);
//		KeyStore truststore = KeyStore.getInstance("JKS");
//        truststore.load(new FileInputStream(TRUSTSTORE_FILE), 
//                                            TRUSTSTORE_PASSWORD.toCharArray());
//        TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
//        tmf.init(truststore);
//        SSLContext sslContext = SSLContext.getInstance("TLS");
//        sslContext.init(null, tmf.getTrustManagers(), null);
//
//        Client client = ClientBuilder.newBuilder()
//                .sslContext(sslContext).build();
//        Response response = client.target(APP_URL).request().get();
//        System.out.println(response.readEntity(String.class));
        
        
		//  como dotar o rest de um contexto SSL para comunicar em HTTPS
        ClientBuilder builder = ClientBuilder.newBuilder();
        SSLContext sslContext = null;
	    try {
	        sslContext = SSLContext.getInstance("SSL");
	        sslContext.init(null, new TrustManager[]{new  SecureTrustManager()}, new SecureRandom());
	    }
	    catch (NoSuchAlgorithmException | KeyManagementException ex) {
	        System.out.println(ex);
	    }
        builder.sslContext(sslContext);
        builder.hostnameVerifier(new HostnameVerifier() {
			
			@Override
			public boolean verify(String hostname, SSLSession session) {
				// devem implementar a verificação do certificado com os dados da sessão SSL
				// pode ser implementado numa class à parte (ver MyHttpURLConnectionFactory)
				return false;
			}
		});
        Client client = builder.build();
        // a partir daqui será igual ao que se faz normalmente com o client.0
        // WebTarget webTarget = client.target(APP_URL);
	}
}
