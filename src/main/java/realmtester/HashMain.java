package realmtester;

import java.io.FileInputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.xml.bind.DatatypeConverter;



public class HashMain {

	
    
	public static void main(String args[]) throws Exception {
		MessageDigest md = null;
		try {
			//1. se usarem mysql sha2('xpto',256) não usem o digest, ou as passwords não irão bater certo.
		md = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
		e.printStackTrace();
		}
		String password = "user";
		byte[] passwordBytes = password.getBytes();
		// ver 1.
		byte[] hash = md.digest(passwordBytes);
		//String hashedPassword = Base64.encodeBytes(hash);
		//DatatypeConverter.printBase64Binary(hash);
		System.out.println(DatatypeConverter.printBase64Binary(hash));
		
		
        
	}
}
