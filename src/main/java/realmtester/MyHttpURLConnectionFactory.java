package realmtester;

import java.net.Proxy;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;

import org.jboss.logging.Logger;

public class MyHttpURLConnectionFactory {
	Proxy proxy;
	String proxyHost;
	Integer proxyPort;
	public boolean canConnect = true;

	public MyHttpURLConnectionFactory() {
	}

	/**
	 *
	 * @return
	 */
	public static SSLContext getSslContext() {
	    SSLContext sslContext = null;
	    try {
	        sslContext = SSLContext.getInstance("SSL");
	        sslContext.init(null, new TrustManager[]{new  SecureTrustManager()}, new SecureRandom());
	    }
	    catch (NoSuchAlgorithmException | KeyManagementException ex) {
	        System.err.println("ERROR OCCURS: " + ex.getLocalizedMessage());
	    }
	    return sslContext;
	}

	/**
	 *
	 * @return
	 */
	public static HostnameVerifier getHostnameVerifier() {
	    return new HostnameVerifier() {
			
			@Override
			public boolean verify(String hostname, SSLSession session) {
				// TODO Auto-generated method stub
				return true;
			}
		};
	}

	public Boolean isHttps(String url) {

	    if (url.startsWith("https://")) {
	        return Boolean.TRUE;
	    }
	    else {
	        return Boolean.FALSE;
	    }
	}
}