package realmtester;

import java.security.Principal;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

// importante que seja javax.faces.bean
@ManagedBean(name="loginBean") 
@SessionScoped
public class LoginBean {

	private String username;  
    private String password;  
    
    
    public String login() {  
         FacesContext context = FacesContext.getCurrentInstance();  
         HttpServletRequest request = (HttpServletRequest) context  
                                            .getExternalContext().getRequest();  
           
         try {  
        	 	  System.out.println("username: "+username);
        	 	 System.out.println("pass: "+password);
        	 	  // irá fazer o login como se fosse o j_security_check
              request.login(username, password);  
         } catch (ServletException e) {  
              // envia para a pagina de erro ! (pode ser alterado)
              return "loginerror";  
         }  
           
         Principal principal = request.getUserPrincipal();  
         System.out.println("Nome: " + principal.getName() );
         
           
           
         if(request.isUserInRole("Manager")) {  
        	 	// vai para a página admin
        	 	  System.out.println("Role confirmado como manager... ");
              return "faces/admin/index?faces-redirect=true";  
         } else {  
        	 	// vai para os users..
        	 	  System.out.println("Não tem role de manager... ");
              return "faces/users/index?faces-redirect=true";  
         }  
    }  
      
    public String logout() {  
         String result="/index?faces-redirect=true";  
           
         FacesContext context = FacesContext.getCurrentInstance();  
         HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();  
           
         try {  
              request.logout();  
         } catch (ServletException e) {  
              System.out.println( "Falha no logout do user " + e);  
              result = "/loginError?faces-redirect=true";  
         }  
           
         return result;  
    }  
 
    public String getUsername() {  
         return username;  
    }  
 
    public void setUsername(String username) {  
         this.username = username;  
    }  
 
    public String getPassword() {  
         return password;  
    }  
 
    public void setPassword(String password) {  
         this.password = password;  
    }  
}
